<?php

use Illuminate\Database\Seeder;

class BlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blocks')->delete();

        $blocks = [
            [
                'id'        => 1,
                'name'      => 'Первый блок',
                'type_id'   => 1
            ],
            [
                'id'        => 2,
                'name'      => 'Второй блок',
                'type_id'   => 2
            ],
            [
                'id'        => 3,
                'name'      => 'Третий блок',
                'type_id'   => 3
            ],
        ];
        DB::table('blocks')->insert($blocks);
    }
}
