<?php

use Illuminate\Database\Seeder;

class BlockFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('block_field')->delete();

        $fields = [
            [
                'id'    => 1,
                'field_id' => 1,
                'block_id' => 1,
                'value' => 'значение 1'
            ],
            [
                'id'    => 2,
                'field_id' => 2,
                'block_id' => 2,
                'value' => 'значение 2'
            ],
            [
                'id'    => 3,
                'field_id' => 3,
                'block_id' => 3,
                'value' => 'значение 3'
            ],
        ];
        DB::table('block_field')->insert($fields);
    }
}
