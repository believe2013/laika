<?php

use Illuminate\Database\Seeder;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fields')->delete();

        $fields = [
            [
                'id'    => 1,
                'name' => 'Первое поле',
                'type_id' => 1
            ],
            [
                'id'    => 2,
                'name' => 'Второе поле',
                'type_id' => 2
            ],
            [
                'id'    => 3,
                'name' => 'Третье поле',
                'type_id' => 3
            ],
        ];
        DB::table('fields')->insert($fields);
    }
}
