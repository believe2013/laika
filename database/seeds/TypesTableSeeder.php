<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->delete();

        $blocks = [
            [
                'id'    => 1,
                'name' => 'Первый тип'
            ],
            [
                'id'    => 2,
                'name' => 'Второй тип'
            ],
            [
                'id'    => 3,
                'name' => 'Третий тип'
            ],
        ];
        DB::table('types')->insert($blocks);
    }
}
