<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public $timestamps = false;

    public function blocks()
    {
        return $this->hasMany('App\Block');
    }

    public function fields()
    {
        return $this->hasMany('App\Field');
    }

    // cascade delete
    protected static function boot() {
        parent::boot();

        static::deleting(function($type) {
            // delete record in pivot table
            foreach ($type->fields()->get() as $field)
                $field->blocks()->detach();
            // delete fields
            $type->fields()->delete();
        });
    }
}
