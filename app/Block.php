<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Field')->withPivot('value');
    }

    // cascade delete
    protected static function boot() {
        parent::boot();

        static::deleting(function($block) {
            //$block->type()->fields();
        });
    }


}
