<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// redirect to types.index
Route::get('/', function()
    {
        return redirect()->route('types.index');
    }
);

/*           TYPES                          */

Route::resource('types', 'TypeController',      ['only' => ['index', 'store', 'edit', 'destroy']]);


/*           FIELDS                         */

Route::resource('fields', 'FieldController',    ['only' => ['store', 'destroy']]);


/*           BLOCKS                         */

Route::resource('blocks', 'BlockController',    ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
