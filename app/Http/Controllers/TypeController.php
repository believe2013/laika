<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

use App\Http\Requests;

class TypeController extends Controller
{
    public function index()
    {
        // all fields
        $types = Type::all();
        
        return view('app.types',['types' => $types]);
    }

    
    public function store(Request $request)
    {
        // validation
        $this->validate($request,
            [
                'name'	=> 'required'
            ]
        );
        
        // create new type
        $type = new Type();
        $type->name = $request['name'];
        $type->save();
        
        // for AJAX
        $response = [
            'id' => $type->id,
            'name' => $type->name
        ];
        return $response;
    }
    

    public function edit($id)
    {
        // type for edit
        $type = Type::where('id', $id)->first();
        
        return view('app.typesEdit',['type' => $type]);
    }

    
    public function destroy($id)
    {
        // delete type
        $type = Type::where('id', $id)->first();
        $type->delete();
    }
    
}
