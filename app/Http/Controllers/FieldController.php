<?php

namespace App\Http\Controllers;

use App\Field;
use App\Type;
use Illuminate\Http\Request;

use App\Http\Requests;

class FieldController extends Controller
{
    
    public function store(Request $request)
    {
        // validation
        $this->validate($request,
            [
                'name'	=> 'required',
                'type'  => 'required|integer'
            ]
        );
        
        // new field
        $field = new Field();
        $field->name = $request['name'];
        $field->type_id = $request['type'];
        $field->save();

        // for field set empty values in pivot table
        $blocksType = Type::find($request['type'])->blocks()->get();
        foreach($blocksType as $k => $v){
            $field->blocks()->attach($v->id, ['value' => '']);
        }
        
        // for AJAX
        $response = [
            'id' => $field->id,
            'name' => $field->name
        ];
        return $response;
    }


    public function destroy($id)
    {
        // delete field
        $field = Field::where('id', $id)->first();
        $field->delete();
    }
    
}
