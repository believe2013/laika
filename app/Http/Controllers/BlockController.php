<?php

namespace App\Http\Controllers;

use App\Block;
use App\Field;
use App\Type;
use Illuminate\Http\Request;

use App\Http\Requests;

class BlockController extends Controller
{
    public function index()
    {
        // all blocks
        $blocks = Block::all();
        // all types for select options
        $types = Type::all();
        return view('app.blocks',['blocks' => $blocks, 'types' => $types]);
    }
    
    
    public function store(Request $request)
    {
        // validate
        $this->validate($request,
            [
                'name'	=> 'required',
                'type'  => 'required|integer'
            ]
        );

        // create new block
        $block = new Block();
        $block->name = $request['name'];
        $block->type_id = $request['type'];
        $block->save();

        // fields for current type
        $fieldsType = Type::find($request['type'])->fields()->get();

        // for fields set empty values in pivot table
        foreach($fieldsType as $k => $v){
            $block->fields()->attach($v->id, ['value' => '']);
        }

        // type name of new block
        $type_name = Type::where('id', $request['type'])->first()->name;

        // for AJAX response (update html)
        $response = [
            'id' => $block->id,
            'name' => $block->name,
            'type' => $type_name
        ];
        return $response;
    }

    public function edit($id)
    {
        // current block
        $block = Block::find($id);

        // get fields
        $fields = $block->fields()->get();

        return view('app.blocksEdit',['block' => $block, 'fields' => $fields]);
    }

    
    public function update($id, Request $request)
    {
        // validate
        $this->validate($request,
            [
                'name'	=> 'required|string',
            ]
        );
        // current block
        $block = Block::find($id);
        // update block
        $block->name = $request['name'];
        $block->save();

        // update fields for current block
        if(count($request['dynamic']) > 0){
            foreach($request['dynamic'] as $k => $v) {
                $field = Field::find($k)->blocks()->find($id);
                $field->pivot->value = $v;
                $field->pivot->save();
            }
        }
        return $field;
    }


    public function destroy($id)
    {
        // current block
        $block = Block::find($id);

        // current type block
        $id_type = $block->type_id;

        // delete pivot values
        if(Type::where('id',$id_type)->count()){
            $fieldsType = Type::find($id_type)->fields()->get();
            foreach($fieldsType as $k => $v){
                $block->fields()->detach($v->id);
            }
        }

        // delete block
        $block->delete();
    }
    
}
