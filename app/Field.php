<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function blocks()
    {
        return $this->belongsToMany('App\Block')->withPivot('value');
    }
    
    
}
