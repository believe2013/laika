@extends('layouts.master')

@section('caption')
    Edit block
@stop

@section('content')
    <form action="{{route('blocks.update', $block->id)}}" method="post" class="blocksUpdate">
        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" class="form-control" name="name" value="{{$block->name}}"><br>
        </div>

        <hr>
        @foreach($fields as $field)
            <div class="form-group">
                <label for="{{$field->id}}">{{$field->name}}</label>
                <input class="form-control" id="{{$field->id}}" name="dynamic[{{$field->id}}]" value="{{$field->pivot->value}}"><br>
            </div>
        @endforeach
        <br>
        {{csrf_field()}}
        <input type="hidden" name="block_id" value="{{$block->id}}">
        <input type="hidden" name="_method" value="PUT">
        <input class="btn btn-primary" type="submit" value="Save">
    </form>
@stop


@section('script')
    <script>
        $(function () {
            // ajax update fields
            $('form.blocksUpdate').on('submit', function (e) {
                e.preventDefault();
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: '{{route('blocks.update', $block->id)}}',
                    data: form.serialize(),
                    success: function(response){
                        console.log(response);
                    }
                });
            });
        });
    </script>
@stop