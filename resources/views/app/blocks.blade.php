@extends('layouts.master')

@section('caption')
    Blocks
@stop

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Type</th>
                <th>Actions</th>
            </tr>
            @foreach($blocks as $block)
                <tr class="item">
                    <td>{{$block->id}}</td>
                    <td>{{$block->name}}</td>
                    <td>{{$block->type['name']}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('blocks.edit', $block->id)}}">Edit</a>
                        <a class="btn btn-danger itemDestroy" href="{{route('blocks.destroy', $block->id)}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <form action="{{route('blocks.store')}}" method="post" class="form-inline itemStore">
        <div class="form-group">
            <input class="form-control" name="name" placeholder="name">
        </div>

        <div class="form-group">
            <select class="form-control" name="type">
                @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                @endforeach
            </select>
        </div>
        {{csrf_field()}}
        <input class="btn btn-success" type="submit" value="Add">
    </form>
@stop


@section('script')
    <script>
        $(function () {
            // ajax delete item
            deleteEntityTable();

            // ajax add item
            $('form.itemStore').on('submit', function (e) {
                e.preventDefault();
                var table = $('table.table');
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function(response){
                        var itemTable = '<tr class="item">' +
                                '<td>' + response.id +'</td>' +
                                '<td>'+ response.name +'</td>' +
                                '<td>'+ response.type +'</td>' +
                                '<td>' +
                                '<a class="btn btn-primary" href="/blocks/'+ response.id +'/edit">Edit</a>' +
                                '<a style="margin-left: 4px" class="btn btn-danger itemDestroy" href="/blocks/'+ response.id +'">Delete</a>' +
                                '</td>' +
                                '</tr>';
                        table.append(itemTable);
                        form.find('input[name="name"]').val('');
                    }
                });
            });
        });
    </script>
@stop