@extends('layouts.master')

@section('caption')
    Types
@stop

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            @foreach($types as $type)
                <tr class="item">
                    <td>{{$type->id}}</td>
                    <td>{{$type->name}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('types.edit', $type->id)}}">Fields</a>
                        <a class="btn btn-danger itemDestroy" href="{{route('types.destroy', $type->id)}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <form action="{{route('types.store')}}" method="post" class="form-inline itemStore">
        <div class="form-group">
            <input name="name" class="form-control" placeholder="Name">
        </div>
        <input type="submit" class="btn btn-success" value="Add">
        {{csrf_field()}}
    </form>
@stop

@section('script')
    <script>
        $(function () {
            // ajax delete item
            deleteEntityTable();

            // ajax add item
            $('form.itemStore').on('submit', function (e) {
                e.preventDefault();
                var table = $('table.table');
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function(response){
                        var itemTable = '<tr class="item">' +
                                        '<td>' + response.id +'</td>' +
                                        '<td>'+ response.name +'</td>' +
                                        '<td>' +
                                            '<a class="btn btn-primary" href="/types/'+ response.id +'/edit">Fields</a>' +
                                            '<a style="margin-left: 4px" class="btn btn-danger itemDestroy" href="/types/'+ response.id +'">Delete</a>' +
                                        '</td>' +
                                        '</tr>';
                        table.append(itemTable);
                        form.find('input[name="name"]').val('');
                    }
                });
            });
        });
    </script>
@stop