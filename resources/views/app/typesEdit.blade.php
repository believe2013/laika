@extends('layouts.master')

@section('caption')
    Fields of type "{{$type->name}}"
@stop

@section('content')
    <table class="table">
        <tbody>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            @foreach($type->fields as $field)
                <tr class="item">
                    <td>{{$field->id}}</td>
                    <td>{{$field->name}}</td>
                    <td>
                        <a class="btn btn-danger itemDestroy" href="{{route('fields.destroy', $field->id)}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <br>
    <form action="{{route('fields.store')}}" method="post" class="form-inline itemStore">
        <div class="form-group">
            <input name="name" class="form-control" placeholder="Name">
        </div>
        <input type="submit" class="btn btn-success" value="Add">
        <input type="hidden" name="type" value="{{$type->id}}">
        {{csrf_field()}}
    </form>
@stop


@section('script')
    <script>
        $(function () {
            // ajax delete item
            deleteEntityTable();

            // ajax add item
            $('form.itemStore').on('submit', function (e) {
                e.preventDefault();
                var table = $('table.table');
                var form = $(this);
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function(response){
                        var itemTable = '<tr class="item">' +
                                '<td>' + response.id +'</td>' +
                                '<td>'+ response.name +'</td>' +
                                '<td>' +
                                    '<a class="btn btn-danger itemDestroy" href="/fields/'+ response.id +'">Delete</a>' +
                                '</td>' +
                                '</tr>';
                        table.append(itemTable);
                        form.find('input[name="name"]').val('');
                    }
                });
            });
        });
    </script>
@stop