<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inlusion test</title>
    <link rel="shortcut icon" href="http://inlu.net/favicon.png">
    <link rel="apple-touch-icon" sizes="60x60" href="http://inlu.net/favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://inlu.net/favicon.png">
    <link rel="manifest" href="http://inlu.net/assets/images/favicons/manifest.json">
    <meta name="csrf-token" content="Iu2UVRnU5EEGVDDPUcUOVVCtqpHUh5zg2KR21byU">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="http://inlu.net/favicon.png">
    <meta name="theme-color" content="#000000">
    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    @include('partials.header')
    <div class="container">
        <div class="row">
            <h1>@yield('caption')</h1>
            <br>
            @yield('content')
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script>
        function deleteEntityTable() {
            $('table').on('click', '.itemDestroy', function (e) {
                e.preventDefault();
                var item = $(this).closest('.item');
                var link = $(this).attr('href');
                $.ajax({
                    type: "POST",
                    url: link,
                    data: '_method=DELETE&_token={{csrf_token()}}',
                    success: function(data){
                        item.remove();
                    }
                });
            });
        }
    </script>
    @yield('script')
</body>
</html>